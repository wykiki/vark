class Main extends hxd.App {
	var bmp:h2d.Bitmap;
	var s2dCenterX:Float;
	var s2dCenterY:Float;

	override function init() {
		// Init center
		s2dCenterX = s2d.width * 0.5;
		s2dCenterY = s2d.height * 0.5;

		// Animation
		var t1 = h2d.Tile.fromColor(0xFF0000, 30, 30);
		var t2 = h2d.Tile.fromColor(0xaa4400, 30, 40);
		var t3 = h2d.Tile.fromColor(0x44aa00, 30, 50);

		var anim = new h2d.Anim([t1, t2, t3], s2d);
		anim.setPosition(s2dCenterX, s2dCenterY);
		anim.speed = 2;

		// Text
		var font = hxd.Res.fonts.gravityFont.toFont();
		var tf = new h2d.Text(font);
		tf.text = "Hello World\nHeaps is great!";
		tf.textAlign = Center;
		tf.setPosition(s2dCenterX, s2dCenterY);
		s2d.addChild(tf);

		// Tile
		// parse Tiled json file
		var mapData:TiledMapData = haxe.Json.parse(hxd.Res.tiles.super_mario_json.entry.getText());

		// get tile image (tiles.png) from resources
		var tileImage = hxd.Res.tiles.super_mario_png.toTile();

		// create a TileGroup for fast tile rendering, attach to 2d scene
		var group = new h2d.TileGroup(tileImage, s2d);

		var tw = mapData.tilewidth;
		var th = mapData.tileheight;
		var mw = mapData.width;
		var mh = mapData.height;

		// make sub tiles from tile
		var tiles = [
			for (y in 0...Std.int(tileImage.height / th))
				for (x in 0...Std.int(tileImage.width / tw))
					tileImage.sub(x * tw, y * th, tw, th)];

		// iterate on all layers
		for (layer in mapData.layers) {
			// iterate on x and y
			for (y in 0...mh)
				for (x in 0...mw) {
					// get the tile id at the current position
					var tid = layer.data[x + y * mw];
					if (tid != 0) { // skip transparent tiles
						// add a tile to the TileGroup
						group.add(x * tw, y * mapData.tilewidth, tiles[tid - 1]);
					}
				}
		}
	}

	override function update(dt:Float) {
		// bmp.rotation += 0.05;
		if (hxd.Key.isReleased(hxd.Key.ESCAPE)) {
			Sys.exit(0);
		}
	}

	static function main() {
		hxd.Res.initEmbed();
		new Main();
	}
}

// simple type definition for Tile map
typedef TiledMapData = {
	layers:Array<{data:Array<Int>}>,
	tilewidth:Int,
	tileheight:Int,
	width:Int,
	height:Int
}
