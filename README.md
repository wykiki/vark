# Vark

Heaps project

## Prerequisite

You need to install `haxe` and `hashlink`.

### Ubuntu (18.04)

#### Haxe

Get the latest `haxe` version (4+).

Copy the `haxe` and `haxelib` binaries into `/usr/local/bin`.

Copy the `std` folder into `/usr/local/lib/haxe/`.

#### Hashlink

Get the latest `hashlink` version (1+).

On Ubuntu, you'll have to compile it from source, and you'll need some
dependencies :

```
sudo apt-get install -y \
	libpng-dev \
	libturbojpeg-dev \
	libvorbis-dev \
	libopenal-dev \
	libsdl2-dev \
	libmbedtls-dev \
	libuv1-dev &&
git clone https://github.com/HaxeFoundation/hashlink.git &&
cd hashlink &&
make &&
sudo make INSTALL_DIR=/usr install
```

## Dependencies

You'll need listed `haxe` dependencies (`haxelib <dependency>`) :
- heaps
- hashlink
- hlsdl

## Run

`haxe app.hxml && hl bin/game.hl`
